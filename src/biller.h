#ifndef BILLER_H
#define BILLER_H

#include <QObject>

class Biller: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString company MEMBER company NOTIFY billerChanged)
    Q_PROPERTY(QString title MEMBER title NOTIFY billerChanged)
    Q_PROPERTY(QString firstname MEMBER firstname NOTIFY billerChanged)
    Q_PROPERTY(QString lastname MEMBER lastname NOTIFY billerChanged)
    Q_PROPERTY(QString street MEMBER street NOTIFY billerChanged)
    Q_PROPERTY(QString houseNumber MEMBER houseNumber NOTIFY billerChanged)
    Q_PROPERTY(QString postalCode MEMBER postalCode NOTIFY billerChanged)
    Q_PROPERTY(QString city MEMBER city NOTIFY billerChanged)
    Q_PROPERTY(QString phoneNumber MEMBER phoneNumber NOTIFY billerChanged)
    Q_PROPERTY(QString faxNumber MEMBER faxNumber NOTIFY billerChanged)
    Q_PROPERTY(QString iban MEMBER iban NOTIFY billerChanged)
    Q_PROPERTY(QString bic MEMBER bic NOTIFY billerChanged)
    Q_PROPERTY(QString bank MEMBER bank NOTIFY billerChanged)
    Q_PROPERTY(QString taxId MEMBER taxId NOTIFY billerChanged)
    Q_PROPERTY(QString currency MEMBER currency NOTIFY billerChanged)

public:
    explicit Biller(QObject *parent = 0): QObject(parent) {}
    ~Biller() {}

    QString company;
    QString title;
    QString firstname;
    QString lastname;
    QString street;
    QString houseNumber;
    QString postalCode;
    QString city;
    QString phoneNumber;
    QString faxNumber;
    QString iban;
    QString bic;
    QString bank;
    QString taxId;
    QString currency;

Q_SIGNALS:
    void billerChanged();
};

#endif
