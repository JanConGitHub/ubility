#ifndef PRODUCTSMODEL_H
#define PRODUCTSMODEL_H

#include <QSqlTableModel>
#include <QSqlRecord>
//#include <QDate>

class Product
{
    Q_GADGET
    Q_PROPERTY(QString title MEMBER title)
    Q_PROPERTY(QString description MEMBER description)
    Q_PROPERTY(double price MEMBER price)
    Q_PROPERTY(double tax MEMBER tax)
    Q_PROPERTY(int id MEMBER id)

public:
    explicit Product() {};
    explicit Product(const QString &title, const QString &description, double price, double tax, int id)
        : title(title), description(description), price(price), tax(tax), id(id)
    {};
    Product(const Product &product)
        : title(product.title), description(product.description), price(product.price), tax(product.tax), id(product.id)
    {};

    QString title;
    QString description;
    double price;
    double tax;
    int id;
};

class ProductsModel : public QSqlTableModel
{
    Q_OBJECT

public:
    explicit ProductsModel(QSqlDatabase database, QObject *parent = 0);
    explicit ProductsModel(const ProductsModel & productsModel) {}
    explicit ProductsModel() {}
    virtual ~ProductsModel();

    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE bool add(const QString &title, const QString &description, double price, double tax);
    Q_INVOKABLE bool remove(int index);
    Q_INVOKABLE Product get(int index) const;

Q_SIGNALS:
    void databaseChanged();

private:
    QSqlRecord createRecord(const QString &title, const QString &description, double price, double tax) const;
};

#endif
