#ifndef UBILITYDATABASE_H
#define UBILITYDATABASE_H

#include <QSqlDatabase>
#include <QPointer>

#include "clientsmodel.h"
#include "billitemsmodel.h"
#include "billsmodel.h"
#include "biller.h"
#include "productsmodel.h"

class UbilityDatabase : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ClientsModel *clients READ clients NOTIFY clientsChanged)
    Q_PROPERTY(BillItemsModel *billItems READ billItems NOTIFY billItemsChanged)
    Q_PROPERTY(BillsModel *bills READ bills NOTIFY billsChanged)
    Q_PROPERTY(ProductsModel *products READ products NOTIFY productsChanged)
    Q_PROPERTY(Client *currentClient READ currentClient NOTIFY currentClientChanged)
    Q_PROPERTY(const Bill &currentBill READ currentBill NOTIFY currentBillChanged)

public:
    explicit UbilityDatabase(QObject *parent = 0);

    Q_INVOKABLE bool removeClient(int index, int clientId);
    Q_INVOKABLE bool setCurrentClient(int index);
    Q_INVOKABLE bool updateCurrentClient(Client *client);
    Q_INVOKABLE bool addClient(Client *client);

    Q_INVOKABLE bool setCurrentBill(int index);
    Q_INVOKABLE QString addBill(Biller *biller);
    Q_INVOKABLE bool cancelBill(int index, int billId);
    Q_INVOKABLE bool undoBill(int index, int billId);
    Q_INVOKABLE QString currentBillToPdf(Biller * biller);

    const Bill & currentBill() const;
    Client * currentClient();
    ClientsModel * clients() const;
    BillItemsModel * billItems() const;
    BillsModel * bills() const;
    ProductsModel * products() const;

Q_SIGNALS:
    void clientsChanged();
    void billItemsChanged();
    void billsChanged();
    void productsChanged();
    void currentClientChanged();
    void currentBillChanged();

private:
    QString currentBillHtml(Biller * biller);

    QSqlDatabase m_database;
    Client m_currentClient;
    QPointer<ClientsModel> m_clients;
    QPointer<BillItemsModel> m_billItems;
    QPointer<BillsModel> m_bills;
    QPointer<ProductsModel> m_products;
};

#endif
