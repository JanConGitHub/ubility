/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Ubility 1.0
import "../components"


ColumnLayout {
    id: productSettings

    anchors {
        fill: parent
        bottomMargin: Qt.inputMethod.visible ? units.gu(2) + Qt.inputMethod.keyboardRectangle.height : units.gu(2)
    }

    property var products: null
    spacing: units.gu(3)

    ScrollView {
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop

        ListView {
            id: productListView
            anchors.fill: parent
            anchors.bottomMargin: units.gu(5)
            footer: Divider {}

            model: products

            delegate: ProductListItem {
                product: model

                onRemove: {
                    UITK_Popups.PopupUtils.open(removeConfirmationComponent)
                }

                //onClicked: {
                //    editProduct(model.index)
                //}

                Component {
                    id: removeConfirmationComponent
                    ConfirmationPopup {
                        onConfirmed: products.remove(model.index)
                        text: i18n.tr("Do you really want to remove the product '%1'?")
                                            .arg(model.title)
                        confirmButtonText: i18n.tr("Delete")
                        confirmButtonColor: UITK.UbuntuColors.red
                    }
                }
            }
        }
    }

    Divider {
        Layout.fillWidth: true
    }

    RowLayout {
        spacing: units.gu(2)
        Layout.fillWidth: true
        Layout.leftMargin: units.gu(2)
        Layout.rightMargin: units.gu(2)

        FormTextInputField {
            id: title
            fieldName: i18n.tr("Product")
            Layout.minimumHeight: units.gu(6)
            Layout.preferredWidth: (parent.width - units.gu(4))/3
            Layout.alignment: Qt.AlignLeft
        }

        FormTextInputField {
            id: price
            fieldName: i18n.tr("Price")
            Layout.minimumHeight: units.gu(6)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }

        FormTextInputField {
            id: tax
            fieldName: i18n.tr("Tax")
            Layout.minimumHeight: units.gu(6)
            Layout.alignment: Qt.AlignRight
            Layout.preferredWidth: (parent.width - units.gu(4))/3
        }
    }

    FormTextInputField {
        id: description
        fieldName: i18n.tr("Description")
        Layout.minimumHeight: units.gu(6)
        Layout.fillWidth: true
        Layout.leftMargin: units.gu(2)
        Layout.rightMargin: units.gu(2)
    }

    UITK.Button {
        text: "Add product"
        color: UITK.UbuntuColors.green
        onClicked: {
            productSettings.products.add(title.text, description.text, price.text, tax.text);
        }
        Layout.alignment: Qt.AlignRight
        Layout.leftMargin: units.gu(2)
        Layout.rightMargin: units.gu(2)
    }
}
