/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtWebEngine 1.7
import Qt.labs.platform 1.0

UITK.Page {
    id: itemsListPage

    property string html: null

    onHtmlChanged: {
        if(html) {
            billView.loadHtml(html, "file:assets/")
        }
    }

    signal printBill()

    header: UITK.PageHeader {
        id: header
        title: i18n.tr("Bill Preview")

        trailingActionBar.actions: [
            UITK.Action {
                id: printBill
                objectName: "printBill"
                text: i18n.tr("Print Bill")
                iconName: "document-print"
                onTriggered: {
                    var cacheDir: "/"+StandardPaths.writableLocation(StandardPaths.CacheLocation).toString().replace(/^(file:\/{3})/,"");
                    billView.printToPdf(cacheDir+"/bill.pdf");
                }
            }
        ]
    }

    WebEngineView {
        id: billView
        anchors.fill: parent
    }
}
