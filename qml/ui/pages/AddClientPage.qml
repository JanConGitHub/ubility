/*
 * Copyright (C) 2019  Johannes Renkl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Ubility 1.0
import "../components"


// TODO: create a person data form component and split this up into two seperate pages
UITK.Page {
    id: addClientPage
    anchors.fill: parent

    property var editedClient: null

    signal addClient(Client client)
    signal updateClient(Client client)

    Client {
        id: newClient
    }

    onEditedClientChanged: {
        if (editedClient) {
            firstname.insert(editedClient.firstname);
            lastname.insert(editedClient.lastname);
            street.insert(editedClient.street);
            houseNumber.insert(editedClient.houseNumber);
            postalCode.insert(editedClient.postalCode);
            city.insert(editedClient.city);
            phoneNumber.insert(editedClient.phoneNumber);
        }
    }

    header: UITK.PageHeader {
        id: header
        title: editedClient ? i18n.tr('Edit Client') : i18n.tr('New Client')

        trailingActionBar.actions: [
            UITK.Action {
                id: addClient
                objectName: "saveClient"
                text: i18n.tr("Save")
                iconName: "tick"
                onTriggered: {
                    if (editedClient) {
                        editedClient.firstname = firstname.text;
                        editedClient.lastname = lastname.text;
                        editedClient.street = street.text;
                        editedClient.houseNumber = houseNumber.text;
                        editedClient.postalCode = postalCode.text;
                        editedClient.city = city.text;
                        editedClient.phoneNumber = phoneNumber.text;
                        addClientPage.updateClient(editedClient);
                    }
                    else {
                        newClient.firstname = firstname.text;
                        newClient.lastname = lastname.text;
                        newClient.street = street.text;
                        newClient.houseNumber = houseNumber.text;
                        newClient.postalCode = postalCode.text;
                        newClient.city = city.text;
                        newClient.phoneNumber = phoneNumber.text;
                        addClientPage.addClient(newClient);
                    }
                    pageLayout.removePages(addClientPage);
                }
            }
        ]
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignVCenter
        spacing: units.gu(3)
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            FormTextInputField {
                id: firstname
                fieldName: i18n.tr('Firstname')
                // TODO: get rid of explicit height here
                Layout.minimumHeight: units.gu(6)
                Layout.preferredWidth: (addClientPage.width-units.gu(6))/2
            }

            FormTextInputField {
                id: lastname
                fieldName: i18n.tr('Lastname')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }   
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            FormTextInputField {
                id: street
                fieldName: i18n.tr('Street')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }   

            FormTextInputField {
                id: houseNumber
                fieldName: i18n.tr('Number')
                Layout.minimumHeight: units.gu(6)
                Layout.minimumWidth: units.gu(10)
            }   
        }

        RowLayout {
            spacing: units.gu(2)
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            FormTextInputField {
                id: postalCode
                fieldName: i18n.tr('Postal code')
                Layout.minimumHeight: units.gu(6)
                Layout.minimumWidth: units.gu(15)
            }   
            
            FormTextInputField {
                id: city
                fieldName: i18n.tr('City')
                Layout.minimumHeight: units.gu(6)
                Layout.fillWidth: true
            }   
        }

        FormTextInputField {
            id: phoneNumber
            fieldName: i18n.tr('Telephone number')
            Layout.minimumHeight: units.gu(6)
            Layout.fillWidth: true
        }   
 
        Rectangle {
            Layout.fillHeight: true
        }
    }
}
